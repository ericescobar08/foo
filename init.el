;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  _____ __  __    _    ____ ____    _       _ _         _
;; | ____|  \/  |  / \  / ___/ ___|  (_)_ __ (_) |_   ___| |
;; |  _| | |\/| | / _ \| |   \___ \  | | '_ \| | __| / _ \ |
;; | |___| |  | |/ ___ \ |___ ___) | | | | | | | |_ |  __/ |
;; |_____|_|  |_/_/   \_\____|____/  |_|_| |_|_|\__(_)___|_|
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(setq debug-on-error t)

;;
;;  ____                                 _   ___        __
;; |  _ \ ___ _ __ ___  ___  _ __   __ _| | |_ _|_ __  / _| ___
;; | |_) / _ \ '__/ __|/ _ \| '_ \ / _` | |  | || '_ \| |_ / _ \
;; |  __/  __/ |  \__ \ (_) | | | | (_| | |  | || | | |  _| (_) |
;; |_|   \___|_|  |___/\___/|_| |_|\__,_|_| |___|_| |_|_|  \___/
;;
;; Personal Information
;;
(setq user-mail-address "eric.escobar08@gmail.com")
(setq user-full-name "Eric Escobar")

;;
;;  _____ _     ____   _
;; | ____| |   |  _ \ / \
;; |  _| | |   | |_) / _ \
;; | |___| |___|  __/ ___ \
;; |_____|_____|_| /_/   \_\
;;
;; Add additional package repositories
;;
(require 'package)
;;(add-to-list 'package-archives
;;             '("marmalade" . "http://marmalade-repo.org/packages/") )
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
;;(add-to-list 'package-archives
;;             '("melpa-stable" . "http://stable.melpa.org/packages/") t)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;;   ____                           _   _     _
;;  / ___| ___ _ __   ___ _ __ __ _| | | |   (_)___ _ __
;; | |  _ / _ \ '_ \ / _ \ '__/ _` | | | |   | / __| '_ \
;; | |_| |  __/ | | |  __/ | | (_| | | | |___| \__ \ |_) |
;;  \____|\___|_| |_|\___|_|  \__,_|_| |_____|_|___/ .__/
;;                                                 |_|
;;  _____                 _   _
;; |  ___|   _ _ __   ___| |_(_) ___  _ __  ___
;; | |_ | | | | '_ \ / __| __| |/ _ \| '_ \/ __|
;; |  _|| |_| | | | | (__| |_| | (_) | | | \__ \
;; |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
;;
;; My general lisp functions
;; Turn off linum mode for current buffer
(defun my/locally-turn-off-linum-mode ()
  "Unconditionally turn off linum mode locally for the current buffer
The primary purpose of this function is to turn off linum mode
when using doc-view-mode since linum mode causes the doc-view to
freeze"
  (interactive)
  (linum-mode -1))

;; Get the current line number
(defun my/current-line-number ()
  "This function will determine the current line number and return that value"
  (count-lines 1 (point)))

;; Determine if the current line is empty or not
(defun my/current-line-empty ()
  "This function is used to determine if the current line the currsor is on in empty"
  (save-excursion
    (beginning-of-line)
    (looking-at "[[:space:]]*$")))

;; Load init file
(defun my/load-init ()
  "If after running a package update or for whatever reason something seems
to be messed up with my environment, this function will reload my init file"
  (interactive)
  (load-file "~/.emacs.d/init.el"))

;;
;;  ____             _                                   _
;; | __ )  __ _  ___| | ____ _ _ __ ___  _   _ _ __   __| |
;; |  _ \ / _` |/ __| |/ / _` | '__/ _ \| | | | '_ \ / _` |
;; | |_) | (_| | (__|   < (_| | | | (_) | |_| | | | | (_| |
;; |____/ \__,_|\___|_|\_\__, |_|  \___/ \__,_|_| |_|\__,_|
;;                       |___/
;; Establish Frame Background
;; Customize Text and Visualizations
(require 'ubuntu-theme)
;; Set the font depending on the system environment
;; Available fonts can be fount with M-x 'list-fontset'
(if (string-equal system-type "windows-nt")
    (set-default-font "-outline-Courier New-normal-normal-normal-mono-17-*-*-*-c-*-fontset-startup"))
(if (string-equal system-type "gnu/linux")
    (set-default-font "-unknown-DejaVu Sans Mono-normal-normal-normal-*-14-*-*-*-m-0-fontset-startup"))

;;            _     _       _ _   _       _    __
;;   ___  ___| |_  (_)_ __ (_) |_(_) __ _| |  / _|_ __ __ _ _ __ ___   ___  ___
;;  / __|/ _ \ __| | | '_ \| | __| |/ _` | | | |_| '__/ _` | '_ ` _ \ / _ \/ __|
;;  \__ \  __/ |_  | | | | | | |_| | (_| | | |  _| | | (_| | | | | | |  __/\__ \
;;  |___/\___|\__| |_|_| |_|_|\__|_|\__,_|_| |_| |_|  \__,_|_| |_| |_|\___||___/
;;
(defun my/set-frame-defaults ()
  (setq default-frame-alist
        '((font . "-outline-Courier New-normal-normal-normal-mono-17-*-*-*-c-*-fontset-startup"))))
;; If the system environment type is windows then set the font (The font is not found in gnu/linux
;; so only do it if in windows)
(if (string-equal system-type "windows-nt")
    (advice-add 'make-frame-command :before 'my/set-frame-defaults))

;;
;;     _
;;    / \__   ___   _
;;   / _ \ \ / / | | |
;;  / ___ \ V /| |_| |
;; /_/   \_\_/  \__, |
;;              |___/
;; Avy stuff
;;
(require 'avy)
(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)
(global-set-key (kbd "<f5>") 'ace-window)
;;(setq aw-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9))
;; Remap the ace window keys to be the asdfjkl keys which
;; are where my fingers naturally sit
(setq aw-keys '(?a ?s ?d ?f ?j ?k ?l))

;;
;;  ____  _              _
;; |  _ \(_)_ __ ___  __| |
;; | | | | | '__/ _ \/ _` |
;; | |_| | | | |  __/ (_| |
;; |____/|_|_|  \___|\__,_|
;;
;; Dired Stuff
;;
;; Allow a dired-jump, which will open a dired buffer with the folder which contains the
;; current file
(require 'dired-x)

;;
;;  ____            _    _                __  __           _
;; |  _ \  ___  ___| | _| |_ ___  _ __   |  \/  | ___   __| | ___
;; | | | |/ _ \/ __| |/ / __/ _ \| '_ \  | |\/| |/ _ \ / _` |/ _ \
;; | |_| |  __/\__ \   <| || (_) | |_) | | |  | | (_) | (_| |  __/
;; |____/ \___||___/_|\_\\__\___/| .__/  |_|  |_|\___/ \__,_|\___|
;;                               |_|
;; Desktop Stuff
;;
(require 'desktop)
(add-to-list 'desktop-path default-directory)
(setq my-desktop-directory default-directory)
(setq my-desktop-file (concat default-directory ".emacs.desktop"))
(if (file-exists-p my-desktop-file)
    (progn
      ;; If the file exists the enable desktop mode
      (desktop-save-mode t)
      ;; Save the desktop file every 300 seconds
      (setq desktop-auto-save-timeout 300)
      (message "Loading desktop file...")
      )
  )

(defun my-save-emacs-desktop-hook ()
  "This function will check to see if desktop mode was enabled before
the emacs session is killed. If it was not enabled it will prompt the
user to save the desktop if desired"
  (interactive)
  (if (not desktop-save-mode)
      ;; Desktop mode was not enabled. Prompt the user to save
      ;; the desktop if desired
      (if (y-or-n-p-with-timeout "Save the desktop environment? " 10 nil)
          (desktop-save my-desktop-directory)
        )
    )
  )
;;  ;; Change desktop directory to directory where
;;  ;; emacs was launched from
;;  ;; It's necessary to do this first otherwise if saving the
;;  ;; desktop is already enabled, then you will be prompted to
;;  ;; save the desktop prior to changing the desktop directory
;;  (desktop-change-dir my-desktop-file)
;;  ;; Enable saving the desktop
;;  ;; When you exit, it will prompt to save the desktop file
;;  (desktop-save-mode t)

;;(setq desktop-load-locked-desktop t)
;;(setq desktop-restore-frames t)
;;(setq desktop-restore-forces-onscreen "all")

(add-hook 'kill-emacs-hook 'my-save-emacs-desktop-hook)


;;
;;   ___              __  __           _
;;  / _ \ _ __ __ _  |  \/  | ___   __| | ___
;; | | | | '__/ _` | | |\/| |/ _ \ / _` |/ _ \
;; | |_| | | | (_| | | |  | | (_) | (_| |  __/
;;  \___/|_|  \__, | |_|  |_|\___/ \__,_|\___|
;;            |___/
;; Org Mode Configurations
;;
(require 'org)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c a") 'org-agenda)
;;(global-set-key (kbd "C-c b") 'org-iswitchb) ; Note sure if I want this one yet

;; Set the org directory
(setq org-directory "~/org")

;; Temporarily placed
;;;;;;;;;; The org cycle agenda files is broken, the following is a temporary fix for it.
(defun org-cycle-agenda-files ()
  "Cycle through the files in `org-agenda-files'.
If the current buffer visits an agenda file, find the next one in the list.
If the current buffer does not, find the first agenda file."
  (interactive)
  (let* ((fs (org-agenda-files t))
         (files (append fs (list (car fs))))
         (tcf (if buffer-file-name (file-truename buffer-file-name)))
         file)
    (unless files (user-error "No agenda files"))
    (catch 'exit
      (while (setq file (pop files))
        (if (equal (file-truename file) tcf)
            (when (car files)
              (find-file (car files))
              (throw 'exit t))))
      (find-file (car fs)))
    (if (buffer-base-buffer) (org-pop-to-buffer-same-window (buffer-base-buffer)))))
;;;;;;;;;;

;; Document Structure
(add-hook 'org-mode-hook 'org-indent-mode) ; Clean outline mode (Properly indents all text)
;; Don't include stars when going to beginning of the line, and don't include tags when going
;; to the end of the line. Just type command twice for absolute movement.
(setq org-special-ctrl-a t)
(setq org-special-ctrl-e t)

;; Establish Personal fast keyboard access to specific sparse trees for agenda views
;; These are just temporary place holders showing the syntax
(setq org-agenda-custom-commands
      '(("w" "Create global TODO list with items marked 'WAITING'" todo "WAITING")))

;; TODO Items (look into tracking state changes)
;; Keeping track of when the state is changed to CANCELLED is incase the
;; item had already been changed to DONE. Normally changing from DONE to
;; CANCELLED would not be recorded thus the '@' is added to the CANCELLED
;; item.
(setq org-todo-keywords
      '((sequence "TODO(t@)" "WAITING(w@)" "|" "DONE(d)")
        (sequence "REPORTED(r)" "BUG(b)" "KNOWNCAUSE(k)" "|" "FIXED(f)")
        (sequence "|" "CANCELED(c@)")
        (sequence "|" "UNASSIGNED(u@)")))
;; Enforce TODO dependencies of parent and children tasks
(setq org-enforce-todo-dependencies t)
;;(setq org-agenda-dim-blocked-tasks t) ;; Don't show blocked tasks
;; Record when a TODO task is closed (When set to note, a time
;; and note are recorded)
(setq org-log-done 'note)
;; Put the state transition notes into a logbook drawer
;; Setting the variable to "LOGBOOK" defines the name of
;; the drawer
(setq org-log-into-drawer "LOGBOOK")

;; Clocking Commands
(defun my-org-update-all-clock-reports-and-export ()
  "Tis function will update all clock reports in the buffer and export the buffer to
an HTML file"
  (interactive)
  (org-update-all-dblocks) ;; Update all dynamic blocks in current buffer
  (execute-kbd-macro (kbd "C-c C-e h o")) ;; Export current buffer to html
  (save-buffer) ;; save the current buffer after the clock reports were updated
  )
;; Assign global key board mapping to run lisp function
;;(global-set-key (kbd "") 'my-org-update-all-clock-reports-and-export)

;; Put multiple clock lines into a wrapped drawer labeld :LOGBOOK:
(setq org-clock-into-drawer t)
;; Establish the time stamp rounding in minutes
(setq org-time-stamp-rounding-minutes (quote (0 5 10 15 20 25 30 35 40 45 50 55)))
;; Customize the idle time (in minutes)
(setq org-clock-idle-time 240)
;; Record a note when clocking out
(setq org-log-note-clock-out t)
;; Only display the time of the current clock instance for the task
(setq org-clock-mode-line-total 'current)
;; When generating a clock report, look into how to remove the
;; \emsp special key
;; Look into this variable =org-pretty-entities= to remove \emsp
;;
;; Update the default display for the clock sum in the clock table report
;; Original Value: (setq org-time-clocksum-format (quote (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))
;; New value (Don't include days)
(setq org-time-clocksum-format (quote (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))
;;
;; TEMP FIX: replace the "\emsp" in org clock report
;; https://emacs.stackexchange.com/questions/9528/is-it-possible-to-remove-emsp-from-clock-report-but-preserve-indentation
(defun my-org-clocktable-indent-string (level)
  (if (= level 1)
      ""
    (let ((str "^"))
      (while (> level 2)
        (setq level (1- level)
              str (concat str "--")))
      (concat str "-> "))))

(advice-add 'org-clocktable-indent-string :override #'my-org-clocktable-indent-string)

;; Org Capure Customization
;; Key map shown above at the start of Org customizations
;; Determine target file for notes (org-directory, is a default directory which
;; is located at "~/org")
(setq org-default-notes-file (concat org-directory "/notes.org"))
;; Establish file where org capture elements will be placed
(setq org-capture-file (concat org-directory "/capture.org"))
;; Playing around with establishing the capture template
(setq org-capture-templates
      '(("x" "My Template" entry (file+headline org-capture-file "Tasks")
         "* TODO %?\n Date Filed: %U\n" :empty-lines 0)
        ("i" "File and start new task" entry (file+headline org-capture-file "Tasks")
         "* TODO %?" :empty-lines 0 :clock-in t)
        ("j" "Journal" entry (file org-capture-file)
         "* Entry Date: %U\n %?" :empty-lines 1 :prepend t)))
;; Define elisp function to open the file where org capture elements are placed
(defun my-get-org-capture-file ()
  "This function is going to be used to open the org-capture-file into a
buffer to be inspected"
  (interactive)
  (find-file org-capture-file))

;; Play with refiling (typically these targets are set to the agenda files)
(setq org-refile-targets
      '((org-agenda-files . (:maxlevel . 3))))
;; Configure settings associated with refiling
(setq org-refile-use-outline-path 'file)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-allow-creating-parent-nodes 'confirm)

;; Archiving (Break out the archive file based on the file they came from
;; chances are each project will be it's own file
(setq org-archive-location (concat org-directory "/archive.org::* From %s"))

;; Turn the time grid off i nthe default agenda view
;; It can be toggled in the agenda view with the key 'G'
(setq org-agenda-use-time-grid nil)

;; Fontify code within the code blocks
(setq org-src-fontify-natively t)

;; Org Export Customization
;; Setting the =org-export-with-sub-superscripts= to the symbol curly braces {}
;; makes the curly braces required in order to trigger the interpretation of
;; '_' and '^' as sub and superscripts. Thus,
;;   hello_world will print with and underscore
;;   hello_{world} will print world as a subscript after 'hello'
(setq org-export-with-sub-superscripts '{})

;;
;;     _              _              __  __           _
;;    / \  _   _  ___| |_ _____  __ |  \/  | ___   __| | ___
;;   / _ \| | | |/ __| __/ _ \ \/ / | |\/| |/ _ \ / _` |/ _ \
;;  / ___ \ |_| | (__| ||  __/>  <  | |  | | (_) | (_| |  __/
;; /_/   \_\__,_|\___|\__\___/_/\_\ |_|  |_|\___/ \__,_|\___|
;;
;; AUCTEX
;;
(setq TeX-auto-save t)
(setq TeX-parse-self t)
;; This is needed to use \include or \input to build a
;; document from multiple documents. This allows you to
;; establish a master document. Very cool!
(setq-default TeX-master nil)

;; Use the standard font for section titles but still provide color
;; highlighting
(setq font-latex-fontify-sectioning 'color)
;;(setq font-latex-slide-title-face 'color)

(setq LaTeX-indent-level 2)
(setq LaTeX-item-indent 0)

;; Unclear things associated with setting the output to be
;; PDF-mode and not DVI-mode
;;(setq TeX-PDF-mode t)
;;(setq-default TeX-PDF-mode t)

;;  ____ ___ _____ _____     _   _ _
;; |  _ \_ _|  ___|  ___|   | | | | |
;; | | | | || |_  | |_ _____| |_| | |
;; | |_| | ||  _| |  _|_____|  _  | |___
;; |____/___|_|   |_|       |_| |_|_____|
;; Use diff-hl package to highlight changed and uncommited lines
(require 'diff-hl)

;; There seem to be some issues when using diff-hl mode with Magit,
;; see the following link <https://github.com/dgutov/diff-hl/issues/65>
;; Therefore the following two diff-hl modes shouldn't be used
;;(diff-hl-dired-mode t)
;;(diff-hl-flydiff-mode t)

(add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
(add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)

;; WHen using Magit, add the hook for diff-hl mode
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)


;;
;;  __  __             _ _
;; |  \/  | __ _  __ _(_) |_
;; | |\/| |/ _` |/ _` | | __|
;; | |  | | (_| | (_| | | |_
;; |_|  |_|\__,_|\__, |_|\__|
;;               |___/
;; Magit configurations
;;
(require 'magit)
;; Global key to get status of git repository
(global-set-key (kbd "C-x g") 'magit-status)
;; Dispatch the magit popup
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

;;  _ _____    _ _ _
;; (_) ____|__| (_) |_
;; | |  _| / _` | | __|
;; | | |__| (_| | | |_
;; |_|_____\__,_|_|\__|
;;
;;
(require 'iedit)

;;  _                _ _
;; (_)___ _ __   ___| | |
;; | / __| '_ \ / _ \ | |
;; | \__ \ |_) |  __/ | |
;; |_|___/ .__/ \___|_|_|
;;       |_|
(require 'ispell)
(add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/")
(setq ispell-program-name "aspell")
;;(setq ispell-personal-dictionary "C:/path/to/your/.ispell")

;;  __  __       _ _   _       _         ____
;; |  \/  |_   _| | |_(_)_ __ | | ___   / ___|   _ _ __ ___  ___  _ __ ___
;; | |\/| | | | | | __| | '_ \| |/ _ \ | |  | | | | '__/ __|/ _ \| '__/ __|
;; | |  | | |_| | | |_| | |_) | |  __/ | |__| |_| | |  \__ \ (_) | |  \__ \
;; |_|  |_|\__,_|_|\__|_| .__/|_|\___|  \____\__,_|_|  |___/\___/|_|  |___/
;;                      |_|
;; multiple-cursors
(require 'multiple-cursors)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
;; allow easy interactive numbering from indexes other than zero
(defun my-mc-insert-numbers (arg)
  (interactive "nStarting Number: ")
  (mc/insert-numbers arg))
(global-set-key (kbd "M-n") 'my-mc-insert-numbers)

(defun my-mc-insert-letters (arg)
  (interactive "nStarting Letter: ")
  (mc/insert-letters arg))
(global-set-key (kbd "M-m") 'my-mc-insert-letters)

(defun my/prompt-for-insert ()
  "prompt for insert allows different test to be inserted for each cursor"
  (interactive)
  (insert (read-string "Insert: ")))
(global-set-key (kbd "M-?") 'my/prompt-for-insert)

(defun my/mc-insert-newline ()
  (interactive)
  (if (my/current-line-empty)
      (progn
        (open-line 1)
        (mc/mark-next-like-this 1)
        (unless (= 2 (mc/num-cursors))
          (progn
            (forward-char)
            (execute-kbd-macro (kbd "C-p"))))
        )
    (open-line 1)
    )
  )
(global-set-key (kbd "C-M->") 'my/mc-insert-newline)

;;
;;                        _                  _
;;  _   _  __ _ ___ _ __ (_)_ __  _ __   ___| |_ ___
;; | | | |/ _` / __| '_ \| | '_ \| '_ \ / _ \ __/ __|
;; | |_| | (_| \__ \ | | | | |_) | |_) |  __/ |_\__ \
;;  \__, |\__,_|___/_| |_|_| .__/| .__/ \___|\__|___/
;;  |___/                  |_|   |_|
;;
;; yasnippet
(require 'yasnippet)
(yas-global-mode 1)
;; Enable nested snippets
(setq yas/triggers-in-field t)
;;
;; suppress backquote warnings in yasnippets
(if (boundp 'warning-suppress-types)
    (add-to-list 'warning-suppress-types '(yasnippet backquote-change))
  (setq warning-suppress-types (list '(yasnippet backquote-change))))
;;
;; Set indenting for all snippets
(setq yas-indent-line 'auto)
;; The above could be done on a file by file basis with the following line
;;# expand-env: ((yas-indent-line 'nil))

;;
;;       _   _
;;   ___| |_| |__   __ _ _ __      __      _____ _ __   __ _  ___ ___
;;  / _ \ __| '_ \ / _` | '_ \ ____\ \ /\ / / __| '_ \ / _` |/ __/ _ \
;; |  __/ |_| | | | (_| | | | |_____\ V  V /\__ \ |_) | (_| | (_|  __/
;;  \___|\__|_| |_|\__,_|_| |_|      \_/\_/ |___/ .__/ \__,_|\___\___|
;;                                              |_|
;;
;; ethan-wspace
;; fix whitespace issues
(require 'ethan-wspace)
(add-hook 'prog-mode-hook 'ethan-wspace-mode)
(setq mode-require-final-newline nil)
(global-set-key [f11] 'ethan-wspace-clean-all)

;;
;;  _   _      _
;; | | | | ___| |_ __ ___
;; | |_| |/ _ \ | '_ ` _ \
;; |  _  |  __/ | | | | | |
;; |_| |_|\___|_|_| |_| |_|
;;
;;
(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-c C-h g") 'helm-google-suggest)
(global-set-key (kbd "C-c C-h o") 'helm-occur)
(define-key minibuffer-local-map (kbd "C-c C-h h") 'helm-minibuffer-history)
(helm-mode 1)
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") 'helm-select-action)

;;
;;   ____ _       _           _   _  __            __  __
;;  / ___| | ___ | |__   __ _| | | |/ /___ _   _  |  \/  | __ _ _ __  ___
;; | |  _| |/ _ \| '_ \ / _` | | | ' // _ \ | | | | |\/| |/ _` | '_ \/ __|
;; | |_| | | (_) | |_) | (_| | | | . \  __/ |_| | | |  | | (_| | |_) \__ \
;;  \____|_|\___/|_.__/ \__,_|_| |_|\_\___|\__, | |_|  |_|\__,_| .__/|___/
;;                                         |___/                |_|
;;
;; Unmap <esc>[ sequence so that it may be used as keypad prefix
                                        ;(global-unset-key "\e[")
;;
;; global mapping to function keys on the PC
;;
(global-set-key (kbd "C-x F") 'find-file-at-point)

;;
;;              _                              _       _
;;   __ _  ___ | |_ ___        _ __ ___   __ _| |_ ___| |__
;;  / _` |/ _ \| __/ _ \ _____| '_ ` _ \ / _` | __/ __| '_ \ _____
;; | (_| | (_) | || (_) |_____| | | | | | (_| | || (__| | | |_____|
;;  \__, |\___/ \__\___/      |_| |_| |_|\__,_|\__\___|_| |_|
;;  |___/
;;
;;               _ __   __ _ _ __ ___ _ __
;;              | '_ \ / _` | '__/ _ \ '_ \
;;              | |_) | (_| | | |  __/ | | |
;;              | .__/ \__,_|_|  \___|_| |_|
;;              |_|
;;
;;
;; jump to matching parentheses
;;   if at paren, jump to match
;;   if between paren, jump to opening paren
;;   if not at or between jump to beginning of buffer
;;
(defun goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis. Else go to the
   opening parenthesis one level up."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1))
        (t
         (backward-char 1)
         (cond ((looking-at "\\s\)")
                (forward-char 1) (backward-list 1))
               (t
                (while (not (looking-at "\\s("))
                  (backward-char 1)
                  (cond ((looking-at "\\s\)")
                         (message "->> )")
                         (forward-char 1)
                         (backward-list 1)
                         (backward-char 1)))))))))
;; Establish Key Binding
(global-set-key (kbd "C-*") 'goto-match-paren)

;;              _                                        _      _
;;   __ _ _   _| |_ ___         ___ ___  _ __ ___  _ __ | | ___| |_ ___
;;  / _` | | | | __/ _ \ _____ / __/ _ \| '_ ` _ \| '_ \| |/ _ \ __/ _ \
;; | (_| | |_| | || (_) |_____| (_| (_) | | | | | | |_) | |  __/ ||  __/
;;  \__,_|\__,_|\__\___/       \___\___/|_| |_| |_| .__/|_|\___|\__\___|
;;                                                |_|
;;
;; auto-complete-mode with pop-up
;;
(global-auto-complete-mode t)


;;
;;  _____ __  __    _    ____ ____
;; | ____|  \/  |  / \  / ___/ ___|
;; |  _| | |\/| | / _ \| |   \___ \
;; | |___| |  | |/ ___ \ |___ ___) |
;; |_____|_|  |_/_/   \_\____|____/
;;
;;                 _   _           _          _   _
;;   ___ _   _ ___| |_(_)_ __ ___ (_)______ _| |_(_) ___  _ __  ___
;;  / __| | | / __| __| | '_ ` _ \| |_  / _` | __| |/ _ \| '_ \/ __|
;; | (__| |_| \__ \ |_| | | | | | | |/ / (_| | |_| | (_) | | | \__ \
;;  \___|\__,_|___/\__|_|_| |_| |_|_/___\__,_|\__|_|\___/|_| |_|___/
;;
;; all settings below this line are managed by emacs
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   (quote
    ("c:/Users/ericesco/Documents/Projects/bigBopperFpgaMckinley.org" "c:/Users/ericesco/Documents/Projects/miscellaneousTasks.org" "c:/Users/ericesco/Documents/Projects/danBoard.org" "c:/Users/ericesco/Documents/Projects/mckinleyDbabfw.org")))
 '(package-selected-packages
   (quote
    (diff-hl transpose-frame yasnippet-snippets magit 2048-game auctex ace-window ace-jump-mode avy bm ethan-wspace expand-region helm iedit multiple-cursors ubuntu-theme yasnippet)))
 '(send-mail-function (quote mailclient-send-it)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;  _____                                           _____
;; |_   _| __ __ _ _ __  ___ _ __   ___  ___  ___  |  ___| __ __ _ _ __ ___   ___
;;   | || '__/ _` | '_ \/ __| '_ \ / _ \/ __|/ _ \ | |_ | '__/ _` | '_ ` _ \ / _ \
;;   | || | | (_| | | | \__ \ |_) | (_) \__ \  __/ |  _|| | | (_| | | | | | |  __/
;;   |_||_|  \__,_|_| |_|___/ .__/ \___/|___/\___| |_|  |_|  \__,_|_| |_| |_|\___|
;;                          |_|
(require 'transpose-frame)

;;  _ _
;; | (_)_ __   ___ _ __  _   _ _ __ ___
;; | | | '_ \ / _ \ '_ \| | | | '_ ` _ \
;; | | | | | |  __/ | | | |_| | | | | | |
;; |_|_|_| |_|\___|_| |_|\__,_|_| |_| |_|
(global-linum-mode 1)
;; Linum-mode causes doc-view-mode to freeze, therefore
;; when in doc-view-mode disable linum mode
(add-hook 'doc-view-mode-hook 'my/locally-turn-off-linum-mode)

;;   __             _
;;  / _| ___  _ __ | |_
;; | |_ / _ \| '_ \| __|
;; |  _| (_) | | | | |_
;; |_|  \___/|_| |_|\__|
;;
(set-face-attribute 'default nil :height 100)

;;  _   _       _             _  __
;; | | | |_ __ (_) __ _ _   _(_)/ _|_   _
;; | | | | '_ \| |/ _` | | | | | |_| | | |
;; | |_| | | | | | (_| | |_| | |  _| |_| |
;;  \___/|_| |_|_|\__, |\__,_|_|_|  \__, |
;;                   |_|            |___/
;; Emacs' standard method for making buffer names unique is to add
;; <2>, <3>, etc. to the end of (all but one of) the buffers. This
;; package replaces that behavior, for buffers visiting files and
;; dired buffers, with a uniquifcation that adds parts of the
;; filename until the buffer names are unique. For instance buffers
;; visiting /home/eescobar/temp/Makefule and /home/jsmith/projects/Makefile
;; would be named Makefile|temp and Makefile|projects respectively
;; (rather than Makefile and Makefile<2>).
;; Other buffer naming styles are available. See help on variable
;; uniquify-buffer-name-style
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)

;;                      _ _   _                      _
;;  ___ _ __ ___   __ _| | | | |___      _____  __ _| | _____
;; / __| '_ ` _ \ / _` | | | | __\ \ /\ / / _ \/ _` | |/ / __|
;; \__ \ | | | | | (_| | | | | |_ \ V  V /  __/ (_| |   <\__ \
;; |___/_| |_| |_|\__,_|_|_|  \__| \_/\_/ \___|\__,_|_|\_\___/
;;
;;
;; small tweaks
;;

;; Softly highlight the line containing point
(global-hl-line-mode t)

;; put the active buffer name and other status in the titlebar
;;
(setq frame-title-format "%*%+ %b -- %m")

;; Undo Tree
(global-undo-tree-mode)

;; Disable Audio Bell
(setq ring-bell-function 'ignore)

;; Display the column number of the current cursor
(setq column-number-mode t)

;; Keep the menubar, however remove the toolbar
;; it adds and extra 2 lines of code into the buffers
(menu-bar-mode t)
(tool-bar-mode 0)

;; Electric Pair mode to insert the matching right element
;; when the left element is inserted. (i.e. insert the matching
;; parenthesis or curly bracket)
(setq electric-pair-mode nil)
(setq electric-pair-pairs '(
                            (?\" . ?\")
                            (?\{ . ?\})
                            (?\( . ?\))
                            (?\[ . ?\])))

;;
;;  ____                   _
;; |  _ \  ___   _____   _(_) _____      __
;; | | | |/ _ \ / __\ \ / / |/ _ \ \ /\ / /
;; | |_| | (_) | (__ \ V /| |  __/\ V  V /
;; |____/ \___/ \___| \_/ |_|\___| \_/\_/
;;
;; Docview
;;
;; Since the Windows OS is lame and doesn't really allow for aliases to exist
;; the doc-view-ghostsciprt-program has to be set explicitly and can't be
;; aliased to the default "gs"
(setq doc-view-ghostscript-program "gswin64.exe")
;;(setq doc-view-pdfdraw-program "C:/EscobarsProgramFiles/mupdf-1.13.0-windows/mupdf.exe")
;; Enable continuous so C-n and C-p will scroll line by line to the next page once
;; the bottom or top of the current page is reached.
(setq doc-view-continuous t)
;; The following functions are used to test emacs' configuration to display
;; different image types and what dlls are required to do so
;;(image-type-available-p 'png)
;;(image-type-available-p 'jpeg)
;;(cdr (assq 'png dynamic-library-alist))


;;
;; Align with spaces only
;;
(defadvice align-regexp (around align-regexp-with-spaces)
  "Never use tabs for alignment."
  (let ((indent-tabs-mode nil))
    ad-do-it))
(ad-activate 'align-regexp)
(setq-default indent-tabs-mode nil)

;; expand region
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;;
;;      _       _          ___     _   _                   __                  _   _
;;   __| | __ _| |_ ___   ( _ )   | |_(_)_ __ ___   ___   / _|_   _ _ __   ___| |_(_) ___  _ __  ___
;;  / _` |/ _` | __/ _ \  / _ \/\ | __| | '_ ` _ \ / _ \ | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
;; | (_| | (_| | ||  __/ | (_>  < | |_| | | | | | |  __/ |  _| |_| | | | | (__| |_| | (_) | | | \__ \
;;  \__,_|\__,_|\__\___|  \___/\/  \__|_|_| |_| |_|\___| |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
;;
;;
;;
;; time-stamp
;;
;; simply put the time-stamp-pattern in your file and it will be updated automatically every file write or save
;; (in this the time-stamp-pattern is "Last Update: <>" and it must occur within the first 40 lines of the file)
;;
(setq time-stamp-format "%:y-%02m-%02d %02H:%02M:%02S %U")
(add-hook 'write-file-hooks 'time-stamp)
(add-hook 'before-save-hook 'time-stamp)
(setq time-stamp-pattern nil)
(setq time-stamp-pattern "40/Last Update:[ \t]+\\\\?[\"<]+%:y-%02m-%02d %02H:%02M:%02S %u\\\\?[\">]")
;;
(defun insert-year ()
  "Insert current year yyyy."
  (interactive)
  (when (region-active-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert (format-time-string "%Y"))
  )
;;
(defun insert-date ()
  "Insert current date yyyy-mm-dd."
  (interactive)
  (when (region-active-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert (format-time-string "%Y-%m-%d"))
  )
;;
(defun insert-date-time ()
  "Insert current date-time string in full
ISO 8601 format.
Example: 2010-11-29T23:23:35-08:00
See: URL `http://en.wikipedia.org/wiki/ISO_8601'
"
  (interactive)
  (when (region-active-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert
   (concat
    (format-time-string "%Y-%m-%dT%T")
    ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
     (format-time-string "%z")))))

;;  __
;; |  |   ___ ___ ___ _ _ ___ ___ ___
;; |  |__| .'|   | . | | | .'| . | -_|
;; |_____|__,|_|_|_  |___|__,|_  |___|
;;               |___|       |___|
;;  _____                     _   _
;; |  _  |___ ___ ___ ___ ___| |_|_|___ ___
;; |   __|  _| . | . | -_|  _|  _| | -_|_ -|
;; |__|  |_| |___|  _|___|_| |_| |_|___|___|
;;               |_|
;;
;; Language Properties

;; Lisp Functions to insert comments
;;
;; Insert a single line comment at point
(defun my/insert-single-line-comment-at-point ()
  "This function will insert a single line comment at the current point location"
  (interactive)
  (insert comment-start) ;; Get the major mode comment character
  (insert " ")) ;; Insert a space after the comment

;; If the point is on an empty line then insert the comment character
;; if not, then go to the next line and insert the comment character
(defun my/insert-single-line-comment ()
  "This function will insert a single line comment"
  (interactive)
  (if (my/current-line-empty)
      (progn
        (beginning-of-line)
        (kill-line)
        (newline)
        (previous-line)
        (move-end-of-line 1)
        (indent-region (my/current-line-number) (+ 1 (my/current-line-number))) ;; Indent the line the cursor is on
        (my/insert-single-line-comment-at-point))
    (progn
      (move-end-of-line 1)
      (newline)
      (indent-region (my/current-line-number) (+ 1 (my/current-line-number))) ;; Indent the line the cursor is on
      (my/insert-single-line-comment-at-point))))

;; Comment out the current line
(defun my/comment-current-line ()
  "This function is used to comment out the current line"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (indent-region (my/current-line-number) (+ 1 (my/current-line-number))) ;; Indent the line the cursor is on
    (my/insert-single-line-comment-at-point)))

;; Establish the global keyboard shortcuts
(global-set-key (kbd "C-<return>") 'my/insert-single-line-comment)
(global-set-key (kbd "S-<return>") 'my/insert-single-line-comment-at-point)
(global-set-key (kbd "C-S-<return>") 'my/comment-current-line)

;;
;; __     __        _ _
;; \ \   / /__ _ __(_) | ___   __ _
;;  \ \ / / _ \ '__| | |/ _ \ / _` |
;;   \ V /  __/ |  | | | (_) | (_| |
;;    \_/ \___|_|  |_|_|\___/ \__, |
;;                            |___/
;; Verilog
;;
(require 'verilog-mode)

;-----------------------------------------------------
;-     Fix underscore to not be part of the word     -
;-----------------------------------------------------
(defun my/verilog-forward-word (&optional arg)
  "This function will move the cursor forward one word and
will not interpret the underscore '_' as part of the word"
  (interactive "p")
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?_ "_" table)
    (with-syntax-table table
      (forward-word arg))))
(defun my/verilog-backward-word (&optional arg)
  "This function will move the cursor backward one word and
will not interpret the underscore '_' as part of the word"
  (interactive "p")
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?_ "_" table)
    (with-syntax-table table
      (backward-word arg))))

(defun my/verilog-beautify-buffer ()
  "This function be used to beautify the entire verilog buffer"
  (interactive)
  (save-excursion
    ;; First align the entire buffer
    (mark-whole-buffer)
    (electric-verilog-tab)
    ;; Next lineup statements across multiple lines
    (goto-char (point-min))
    (while (< (point) (point-max))
      (verilog-pretty-expr)
      (next-line))
    (verilog-pretty-expr)))

(setq verilog-align-ifelse nil)
(setq verilog-auto-lineup 'all)
(setq verilog-indent-begin-after-if t)
(setq verilog-indent-declaration-macros nil);
(setq verilog-indent-level             2)
(setq verilog-indent-level-module      0) ;; Affects the indentation of always block
(setq verilog-indent-level-declaration 0)
(setq verilog-indent-level-directive   0)
(setq verilog-indent-level-behavioral  0)
(setq verilog-auto-newline nil)
(setq verilog-auto-indent-on-newline t)
(setq verilog-minimum-comment-distance 20)
; ;; User customization for Verilog mode
;       verilog-indent-level-module      3
;       verilog-indent-level-declaration 3
;       verilog-indent-level-behavioral  3
;       verilog-indent-level-directive   1
;       verilog-case-indent              2
;       verilog-auto-newline             t
;       verilog-auto-indent-on-newline   t
;       verilog-tab-always-indent        t
;       verilog-auto-endcomments         t
;       verilog-minimum-comment-distance 40
;       verilog-indent-begin-after-if    t
;       verilog-auto-lineup              '(all))

(defun my/verilog-setup ()
  "My post verilog mode setup"
  (interactive)
  (local-set-key (kbd "M-<right>") 'my/verilog-forward-word)
  (local-set-key (kbd "M-<left>") 'my/verilog-backward-word)
  (local-set-key (kbd "C-c b") 'my/verilog-beautify-buffer)
  )
(add-hook 'verilog-mode-hook 'my/verilog-setup)

;;
;; __     ___   _ ____  _
;; \ \   / / | | |  _ \| |
;;  \ \ / /| |_| | | | | |
;;   \ V / |  _  | |_| | |___
;;    \_/  |_| |_|____/|_____|
;;
;; VHDL
;;
(require 'vhdl-mode)
;; set the style
(defun my-vhdl-mode-hook ()
  ;; use IEEE style for all VHDL code
  (vhdl-set-style "IEEE")
  ;; other customizations can go here
  ;; Change the indentation
  (setq vhdl-basic-offset 2)
  )
(add-hook 'vhdl-mode-hook 'my-vhdl-mode-hook)

;; Enable template insertion
(setq vhdl-electric-mode t)
;; Enable emacs stuttering
(setq vhdl-stutter-mode t)
;; Online sample VHDL mode customization
;;  -- (defconst my-vhdl-style
;;  --   '((vhdl-tab-always-indent        . t)
;;  --     (vhdl-comment-only-line-offset . 4)
;;  --     (vhdl-offsets-alist            . ((arglist-close    . vhdl-lineup-arglist)
;;  --                                       (statement-cont   . 0)
;;  --                                       (case-alternative . 4)
;;  --                                       (block-open       . 0)))
;;  --     (vhdl-echo-syntactic-information-p . t)
;;  --     )
;;  --   "My VHDL Programming Style")
;;  --
;;  -- ;; Customizations for vhdl-mode
;;  -- (defun my-vhdl-mode-hook ()
;;  --   ;; add my personal style and set it for the current buffer
;;  --   (vhdl-add-style "PERSONAL" my-vhdl-style t)
;;  --   ;; offset customizations not in my-vhdl-style
;;  --   (vhdl-set-offset 'statement-case-intro '++)
;;  --   ;; other customizations
;;  --   (setq tab-width 8
;;  --         ;; this will make sure spaces are used instead of tabs
;;  --         indent-tabs-mode nil)
;;  --   ;; keybindings for VHDL are put in vhdl-mode-map
;;  --   (define-key vhdl-mode-map "\C-m" 'newline-and-indent)
;;  --   )
;;  --
;;  -- (add-hook 'vhdl-mode-hook 'my-vhdl-mode-hook)

;;   ____
;;  / ___|
;; | |
;; | |___
;;  \____|
;;
;; c-style syntax
(defun my-c-align-defines-buffer ()
  "This function will align all of the #defines within a buffer.
The start of the #define names will be aligned and then the text
to be replaced will be aligned for every #define statement"
  (interactive)
  (my-c-align-defines-region (point-min) (point-max)))

(defun my-c-align-defines-region (beg end)
  "This function will align all of the #define statements within
a given region"
  (interactive "r")
  (align-regexp beg end "^#define\\([[:space:]]+\\)\\w")
  (align-regexp beg end "^#define[[:space:]]\\(\\w\\|_\\)*\\([[:space:]]+\\)[0-9]" 2))

;;   ____
;;  / ___| _     _
;; | |   _| |_ _| |_
;; | |__|_   _|_   _|
;;  \____||_|   |_|
;;
;; C++

;; This hack fixes indentation for C++11's "enum class" in Emacs.
;; http://stackoverflow.com/questions/6497374/emacs-cc-mode-indentation-problem-with-c0x-enum-class/6550361#6550361
(defun inside-class-enum-p (pos)
  "Checks if POS is within the braces of a C++ \"enum class\"."
  (ignore-errors
    (save-excursion
      (goto-char pos)
      (up-list -1)
      (backward-sexp 1)
      (looking-back "enum[ \t]+class[ \t]+[^}]+"))))
(defun align-enum-class (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      0
    (c-lineup-topmost-intro-cont langelem)))
(defun align-enum-class-closing-brace (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      '-
    '+))
(defun fix-enum-class ()
  "Setup `c++-mode' to better handle \"class enum\"."
  (add-to-list 'c-offsets-alist '(topmost-intro-cont . align-enum-class))
  (add-to-list 'c-offsets-alist
               '(statement-cont . align-enum-class-closing-brace)))
(add-hook 'c++-mode-hook 'fix-enum-class)

;; Set indentation style to be linux with a 2 space indentation offset
;; It was originally 4 however changed to 2 per Larry York's request
(setq c-default-style "linux"
      c-basic-offset 2)

;; Switch-Case Statement Indenting
(c-set-offset 'case-label '+)

;;  __  __   _ _____ _      _   ___
;; |  \/  | /_\_   _| |    /_\ | _ )
;; | |\/| |/ _ \| | | |__ / _ \| _ \
;; |_|  |_/_/ \_\_| |____/_/ \_\___/
;; matlab
(load-library "matlab")
(matlab-cedet-setup)
(add-hook 'matlab-mode
          (lambda ()
            (auto-complete-mode 1)
            ))

(add-hook 'matlab-mode-hook
          (lambda () (local-set-key [C-tab] 'indent-for-tab-command))) ; code alignment
(add-hook 'matlab-mode-hook
          (lambda () (local-set-key [S-tab] 'hippie-expand))) ; map auto completion back to TAB key
(add-hook 'matlab-mode-hook
          (lambda () (local-set-key [M-return] 'bm-toggle))) ; Alt-Enter

(autoload 'matlab-mode "matlab" "Matlab Editing Mode" t)
(add-to-list
 'auto-mode-alist
 '("\\.m$" . matlab-mode))
(setq matlab-indent-function t)
(setq matlab-shell-command "matlab")

;;  _    _           __      __                  _
;; | |  (_)_ _  ___  \ \    / / _ __ _ _ __ _ __(_)_ _  __ _
;; | |__| | ' \/ -_)  \ \/\/ / '_/ _` | '_ \ '_ \ | ' \/ _` |
;; |____|_|_||_\___|   \_/\_/|_| \__,_| .__/ .__/_|_||_\__, |
;;                                    |_|  |_|         |___/
;; line wrapping
(setq-default truncate-lines t)
(add-hook 'org-mode-hook 'turn-on-auto-fill)
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'latex-mode-hook (lambda () (setq truncate-lines nil)))
(add-hook 'text-mode-hook (lambda () (setq truncate-lines nil)))
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))

;;
;;  _
;; | |__  _ __ ___
;; | '_ \| '_ ` _ \
;; | |_) | | | | | |
;; |_.__/|_| |_| |_|
;;
;; Visible Book Mark
;; visible bm
;;
(require 'bm)
(global-set-key (kbd "<f2>") 'bm-toggle)
(global-set-key (kbd "M-<down>") 'bm-next)
(global-set-key (kbd "M-<up>") 'bm-previous)
(global-set-key (kbd "M-S-<f2>") 'bm-remove-all-all-buffers)

;;
;; Random functionality written by yours truely
;;
;; Assign a global key map (I really want to add this to a major mode)
(global-set-key (kbd "C-c r h") 'my-replace-decimal-with-hexadecimal)
;; Assign a global key map (I really want to add this to a major mode)
(global-set-key (kbd "C-c r b") 'my-replace-decimal-with-binary)
(global-set-key (kbd "C-c r d") 'my-replace-hexadecimal-with-decimal)

;; Convert a decimal number to binary
(defun my-decimal-to-binary (decimalValue numberOfBits)
  "This function takes a decimal value as an
input and returns a string which expresses the
number in binary"
  (let (binaryValue result base)
    (setq base 2)
    (setq binaryValue nil)
    (setq result decimalValue)
    (while (/= result 0)
      (setq binaryValue (concat (number-to-string (% result base)) binaryValue))
      (setq result (/ result base)))
    ;; Zero pad to the desired number of bits if necessary
    (while (< (length binaryValue) numberOfBits)
      (setq binaryValue (concat "0" binaryValue)))
    binaryValue))

;; Convert a decimal number to hexadecimal
(defun my-decimal-to-hexadecimal (decimalValue numberOfCharacters)
  "This function takes a decimal value as an input
and returns a string which expresses the number in
hexadecimal"
  (interactive)
  (let (hexValue result base)
    (setq base 16)
    (setq hexValue nil)
    (setq result decimalValue)
    (while (/= result 0)
      (setq hexValue (concat (my-get-hexadecimal-character (% result base)) hexValue))
      (setq result (/ result base)))
    ;; Zero Pad to the desired number fo characters if necessary
    (while (< (length hexValue) numberOfCharacters)
      (setq hexValue (concat "0" hexValue)))
    hexValue))

;; Replace decimal number with the binary number
(defun my-replace-decimal-with-binary (numBits)
  "This function will replace the decimal value in the
active region with its binary representation"
  (interactive "P")
  (let (start end decimalValue)
    (setq start (region-beginning))
    (setq end (region-end))
    (setq decimalValue (string-to-number (buffer-substring-no-properties start end)))
    ;; Replace text with new binary representation
    (delete-region start end)
    (goto-char start)
    ;; Default number of bits is 4
    (unless numBits
      (setq numBits 4))
    (insert (my-decimal-to-binary decimalValue numBits))))

;; Replace decimal number with the hexadecimal number
(defun my-replace-decimal-with-hexadecimal (numChar)
  "This function will replace the decimal value in the
 active region with its hexadecimal representation"
  (interactive "P")
  (let (start end decimalValue)
    (setq start (region-beginning))
    (setq end (region-end))
    (setq decimalValue (string-to-number (buffer-substring-no-properties start end)))
    ;; Replace text with hexadecimal representation
    (delete-region start end)
    (goto-char start)
    ;; Default number of hexadecimal characters is 4
    (unless numChar
      (setq numChar 4))
    (insert (my-decimal-to-hexadecimal decimalValue numChar))))

;; get the single hexadecimal character for a decimal value
(defun my-get-hexadecimal-character (decimalValue)
  "This function will return the single hexadecimal character
for the input decimal value. The decimal value must be
less than 16"
  (unless (< decimalValue 16)
    (error (message "Input value must be less than 16")))
  (unless (>= decimalValue 0)
    (error (message "Input value must be greater than or equal to 0")))
  (let (hexChar)
    (if (< decimalValue 10)
        ;; If less than 10 then just return the decimal as an alphanumeric character
        (setq hexChar (number-to-string decimalValue))
      ;; Offset the decimal value by 10 Since A = 10 in hex but I'm
      ;; adding to the converted ascii decimal value
      (setq hexChar (char-to-string (+ (- decimalValue 10) (string-to-char "A")))))
    hexChar))

;; Replace the hexadecimal value with the decimal
(defun my-replace-hexadecimal-with-decimal ()
  "This function will replace the hexadecimal value
in the active region with its decimal representation"
  (interactive)
  (let (start end hexValue)
    (setq start (region-beginning))
    (setq end (region-end))
    (setq hexValue (buffer-substring-no-properties start end))
    ;; Replace the ext with decimal
    (delete-region start end)
    (goto-char start)
    (insert (number-to-string (string-to-number hexValue 16)))))

;; Print the decimal value of the hex string
(defun my-what-hexadecimal-value ()
  "Prints the deciaml value of a hexadecimal string under cursor
Samples of valid inputs:

ffff
FFFF
0xffff (C)
0xFFFF
#xffff (elisp)
#xFFFF"
  (interactive)

  (let (inputStr start end)
    ;; Save the location of the cursors location
    (save-excursion
      ;; Find the start of the hex string
      (re-search-backward "[^0-9A-Fa-fx#]" nil t)
      (forward-char)
      (setq start (point))
      ;; Find the end of the hex string
      (re-search-forward "[^0-9A-Fa-fx#]" nil t)
      (backward-char)
      (setq end (point)))

    (setq inputStr (buffer-substring-no-properties start end))

    (let ((case-fold-search nil))
      (setq tempStr (replace-regexp-in-string "^0x"   "" inputStr)) ; C, Perl, …
      (setq tempStr (replace-regexp-in-string "^#x"   "" tempStr )) ; elisp …
      (setq tempStr (replace-regexp-in-string "^#"    "" tempStr )) ; CSS …
      (setq tempStr (replace-regexp-in-string "^16'h" "" tempStr )) ; Verilog
      )

    (message "Hex %s is %d" tempStr (string-to-number tempStr 16))))

;; Print the decimal value of the binary string
(defun my-what-binary-value ()
  "Prints the deciaml value of a binary string under cursor
Samples of valid inputs:

0010
1111
0b0011 (C)
0b1101
#b0010 (elisp)
#b0110
16'b0011001100110011 (Verilog)
8'b00110011"
  (interactive)

  (let (inputStr start end)
    ;; Save the location of the cursors location
    (save-excursion
      ;; Find the start of the hex string
      (re-search-backward "[^0-9#]" nil t)
      (forward-char)
      (setq start (point))
      ;; Find the end of the hex string
      (re-search-forward "[^0-9]" nil t)
      (backward-char)
      (setq end (point)))

    (setq inputStr (buffer-substring-no-properties start end))

    (let ((case-fold-search nil))
      (setq tempStr (replace-regexp-in-string "^0b"       "" inputStr)) ; C, Perl, …
      (setq tempStr (replace-regexp-in-string "^#b"       "" tempStr )) ; elisp …
      (setq tempStr (replace-regexp-in-string "^#"        "" tempStr )) ; CSS …
      (setq tempStr (replace-regexp-in-string "^[0-9]*'b" "" tempStr )) ; Verilog
      )

    (message "Binary %s is %d" tempStr (string-to-number tempStr 2))))

;;                  _                       _
;;   _____   ____ _| |       __ _ _ __   __| |
;;  / _ \ \ / / _` | |_____ / _` | '_ \ / _` |_____
;; |  __/\ V / (_| | |_____| (_| | | | | (_| |_____|
;;  \___| \_/ \__,_|_|      \__,_|_| |_|\__,_|
;;                         _
;;          _ __ ___ _ __ | | __ _  ___ ___
;;         | '__/ _ \ '_ \| |/ _` |/ __/ _ \
;;         | | |  __/ |_) | | (_| | (_|  __/
;;         |_|  \___| .__/|_|\__,_|\___\___|
;;                  |_|
;; Some weird eval and replace shananiganz
;; Found this at
;; http://emacsredux.com/blog/2013/06/21/eval-and-replace/
(defun eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))

(global-set-key (kbd "C-c e") 'eval-and-replace)
;;  _____         _
;; | __  |___ ___| |_ _ _ ___
;; | __ -| .'|  _| '_| | | . |
;; |_____|__,|___|_,_|___|  _|
;;                       |_|
;;  ____  _             _
;; |    \|_|___ ___ ___| |_ ___ ___ _ _
;; |  |  | |  _| -_|  _|  _| . |  _| | |
;; |____/|_|_| |___|___|_| |___|_| |_  |
;;                                 |___|
;; The following sets the auto backup
;; files to be saved in a backup directory
(setq backup-directory-alist
      `((".*" . ,"~/.emacs.d/backup/")))
(setq auto-save-file-name-transforms
      `((".*" ,"~/.emacs.d/backup/" t)))

;;  _           _                                       _ _
;; | |__  _   _| |_ ___        ___ ___  _ __ ___  _ __ (_) | ___
;; | '_ \| | | | __/ _ \_____ / __/ _ \| '_ ` _ \| '_ \| | |/ _ \
;; | |_) | |_| | ||  __/_____| (_| (_) | | | | | | |_) | | |  __/
;; |_.__/ \__, |\__\___|      \___\___/|_| |_| |_| .__/|_|_|\___|
;;        |___/                                  |_|
;; Recompile .el files as necessary
;; Uncomment out the line below or juse execute it
;; to compile all the .el files into .elc files
;;(byte-recompile-directory "~/.emacs.d/elpa" 0)
